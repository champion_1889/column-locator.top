<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ExcelGeneratorService;

class ExcelController extends Controller
{
    public function getExcel(Request $request){
        $exl = new ExcelGeneratorService();
        $exl->generate($request->user_id);
    }
}
