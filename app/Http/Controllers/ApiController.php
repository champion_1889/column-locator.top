<?php

namespace App\Http\Controllers;

use App\Models\Columns;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Validator;

class ApiController extends Controller
{
    public $successStatus = 200;

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        if(Auth::attempt(['email' => request('username'), 'password' => request('password')]) ||
           Auth::attempt(['username' => request('username'), 'password' => request('password')])
        ){
            $user = Auth::user();
            $success['user'] = [
                'name' => $user->name,
                'email' => $user->email,
                'role' => $user->role,
                'username' => $user->username
            ];
            $success['token'] = $user->createToken('MyApp')-> accessToken;
            return response()->json(['success' => $success], $this-> successStatus);
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    public function loginByUsername(Request $request){
        $user = User::where('username', '=', $request->get('username'))->first();
        if($user) {
            Auth::setUser($user);
            $success['user'] = $user;
            $success['token'] = Auth::user()->createToken('MyApp')->accessToken;
            return response()->json(['success' => $success], $this->successStatus);
        }else{
            return response()->json(['success'=>false], 200);
        }
    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'username' => 'required|username',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')-> accessToken;
        $success['name'] =  $user->name;
        return response()->json(['success'=>$success], $this-> successStatus);
    }

    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }

    public function getUsersList(Request $request){
        return User::all();
    }

    public function addColumn(Request $request){
        $column = new Columns();
        $column->user_id = Auth::id();
        $column->street = $request->get('street');
        $column->note = $request->get('note');
        $column->latitude = $request->get('latitude');
        $column->longitude = $request->get('longitude');
        $res = $column->save();

        return response()->json(['success' => $res], $this->successStatus);
    }
}
