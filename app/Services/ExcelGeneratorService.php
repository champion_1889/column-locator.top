<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ExcelGeneratorService
{
    public function generate($user_id = 0){
        $tables = $this->getColumns($user_id);
        Excel::create('ColumnsLocations.xls', function ($excel) use ($tables){
                foreach ($tables as $key => $item){
                    $excel->sheet($key, function($sheet) use ($item){
                        $sheet->cells('A1:E1', function($cells) {
                            $cells->setBackground('#c2c2c2');
                            $cells->setFontWeight('bold');
                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                            $cells->setAlignment('center');
                        });
                        $sheet->fromArray($item);
                    });
                }
            })->export('xls');
    }

    private function getColumns($user_id = null){
        $tables = [];
        if($user_id == null) {
            $columns = DB::table('columns')
                ->select('users.username', 'users.name', 'columns.street', 'columns.latitude', 'columns.longitude', 'columns.note', 'columns.created_at')
                ->join('users', 'users.id', '=', 'columns.user_id')
                ->get();
        }else{
            $columns = DB::table('columns')
                ->select('users.username', 'users.name', 'columns.street', 'columns.latitude', 'columns.longitude', 'columns.note', 'columns.created_at')
                ->join('users', 'users.id', '=', 'columns.user_id')
                ->where('columns.user_id', '=', $user_id)
                ->get();
        }
        foreach ($columns as $key => $item) {
            $tables[$item->username . " - " . $item->name][] =
                ['Latitude' => $item->latitude,
                    'Longitude' => $item->longitude,
                    'Street' => $item->street,
                    'Note' => $item->note,
                    'Date' => $item->created_at];
        }
        return $tables;
    }
}