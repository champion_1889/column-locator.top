<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * @property $id
 * @property $user_id
 * @property $latitude
 * @property $longitude
 * @property $note
 * @property $street
 * @property $created_at
 * @property $updated_at
 */
class Columns extends Model
{
    protected $table = 'columns';
}