<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function(){
    Route::post('details', 'ApiController@details');
    Route::get('users', 'ApiController@getUsersList');
    Route::post('column', 'ApiController@addColumn');
    Route::post('login/username', 'ApiController@loginByUsername');
});

Route::post('login', 'ApiController@login');
Route::post('register', 'ApiController@register');